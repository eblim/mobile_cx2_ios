//
//  IntroViewController.swift
//  hrc
//
//  Created by Eun Bi Lim on 2020/01/10.
//  Copyright © 2020 Eun Bi Lim. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import SwiftyJSON
import Photos
import AdSupport
import CoreTelephony
import Firebase

class IntroViewController: UIViewController {
    
    var delegate: ViewController?
    
    let imagePickerController = UIImagePickerController()
    
    let userData: UserData = UserData.shared
    var isVersionPopShow : Bool = false
    
    override func loadView() {
       super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 알림권한 (필수?? 선택??) 동기처리 필요 (뭐부터??)
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (result, Error) in
            print("Permission YN : \(result)")
        }
        
        // 사진권한 (앱 진입시마다??)
        let photoStatus = PHPhotoLibrary.authorizationStatus()
        print("Photo permission : ", photoStatus)
        
        PHPhotoLibrary.requestAuthorization({ (photoStatus) in
            if photoStatus == PHAuthorizationStatus.denied || photoStatus == PHAuthorizationStatus.notDetermined {
                self.setAuthAlertAction()
            }
        })
        
        if  userData.getUserId() != "" && userData.getUserPw() != "" {
            getLogin()
        } else {
            if userData.getPushId() != "" {
                setDeviceInfo()
            } else {
                getToken()
            }
        }
    }
    
    func setAuthAlertAction() {
        let authAlertController: UIAlertController
        authAlertController = UIAlertController(title: "사진첩 권한 요청", message: "사진첩 권한을 허용하여 설문조사에 응할 수 있습니다.", preferredStyle: UIAlertController.Style.alert)
        
        let confirmAction: UIAlertAction
        confirmAction = UIAlertAction(title: "허용", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            if let appSettings = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
            }
        })
        
        let cancelAction: UIAlertAction
        cancelAction = UIAlertAction(title: "거부", style: UIAlertAction.Style.default, handler: nil)
        
        authAlertController.addAction(confirmAction)
        authAlertController.addAction(cancelAction)
        self.present(authAlertController, animated: true, completion: nil)
    }
    
    func getToken() {
        
        InstanceID.instanceID().instanceID { (result, error) in
          if let error = error {
            print("Error fetching remote instance ID: \(error)")
          } else if let result = result {
            print("Remote instance ID token: \(result.token)")
            UserDefaults.standard.set(result.token, forKey: "pushId")
            self.setDeviceInfo()
          }
        }
    }
    
    func returnToView() {
        self.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "IntroViewSegue", sender: (Any).self)
    }

    func getLogin() {
        let loginUrl = "https://cxapi.hrcglobal.com/api/MobileCX/Login/?userID=\(userData.getUserId())&userPWD=\(userData.getUserPw())"
            
            let alamo = Alamofire.request(loginUrl, method: .get, parameters: nil, encoding: URLEncoding.httpBody)
            alamo.responseJSON { response in

                if response.response?.statusCode  == 200 {
                    if response.result.value != nil {
                        let json = JSON(response.result.value!)
                        if  json["loginMessage"] == "Success" {
                            let cookie = response.response?.allHeaderFields["Set-Cookie"] as! String
                            let userId = json["UserID"].string
                            let pid = json["Pid"].string
                            let userName = json["UserName"].string
                            let userEmail = json["UserEmail"].string
                            self.userData.saveUserData(userId: userId!, userPid: pid!, userName: userName!, userEmail: userEmail!)
                            UserDefaults.standard.set(cookie, forKey: "cookie")
                            UserDefaults.standard.synchronize()
                            self.setDeviceInfo()
                        }
                    }
                } else {
                    self.setDeviceInfo()
                }
            }
    }
    
    func setDeviceInfo() {
        let deviceSettingUrl = "https://cxapi.hrcglobal.com/api/MobileCX/DeviceSetting"
        var version: String {
            let dictionary = Bundle.main.infoDictionary
            let version = dictionary!["CFBundleShortVersionString"]
            return version as! String
        }
        
        let params: [String: Any] = [
            "loginId" : userData.getUserId(),
            "pid" : userData.getUserPid(),
            "deviceId" : UIDevice.current.identifierForVendor?.uuidString as! String,
            "osType" : "IOS",
            "osVersion" : UIDevice.current.systemVersion,
            "androidId" : "",
            "adId" : ASIdentifierManager.shared().advertisingIdentifier,
            "serialNumber" : "",
            "msisdn" : "",
            "model" : IntroViewController.getDeviceIdentifier(),
            "telecom" : CTTelephonyNetworkInfo.init().subscriberCellularProvider?.carrierName!,
            "nation" : Locale.current.regionCode!,
            "pushId" : userData.getPushId(),
            "appVersion" : version,
            "appVersionCode" : "",
            "appSettingYn" : "",
            "messageSettingYn" : "",
            "locationSettingYn" : "",
            "pushYn" : "",
            "phoneYn" : "",
            "mediaYn" : "",
            "micYn" : "",
            "cameraYn" : ""
        ]
        
        print("Parameters : \(params)")
        
        let alamo = Alamofire.request(deviceSettingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody)
        alamo.responseJSON { response in

            if response.response?.statusCode  == 200 {
                let json = JSON(response.result.value!)
                print("Response : \(json)")
                if  json["result"] == "OK" {
                    let latestAppVersion = json["appVersion"].string
                    let forceUpdateYn = json["forceUpdateYn"].string
                    self.compareVersion(insallVersion: version, latestVersion: latestAppVersion!, forceUpdateYn: forceUpdateYn!)
                } else {
                    self.returnToView()
                }
            } else {
                self.returnToView()
            }
        }
    }
    
    func compareVersion(insallVersion: String, latestVersion: String, forceUpdateYn: String){
        let myLatestVersion = latestVersion.replacingOccurrences(of: ".", with: "")
        let testVersion = "1.0.0".replacingOccurrences(of: ".", with: "")
        let myInstallVersion = insallVersion.replacingOccurrences(of: ".", with: "")
        print("@@@ 버전 비교, latest=\(myLatestVersion), my=\(myInstallVersion)")
        if testVersion > myInstallVersion {
            print("@@@ 앱 업데이트 필요")
//            if forceUpdateYn == "true" {
//                let message = "최신버전의 앱이 있습니다. 업데이트 이후에 사용 가능합니다."
//                let alertController = UIAlertController(title: "MobileCX 업데이트", message: message, preferredStyle: .alert)
//                let yes = UIAlertAction(title: "업데이트", style: .default, handler: updateAppHandler)
//                alertController.addAction(yes)
//                present(alertController, animated: true, completion: nil)
//            } else {
                let message = "최신버전의 앱이 있습니다. 앱을 업데이트하시겠습니까?"
                let alertController = UIAlertController(title: "MobileCX 업데이트", message: message, preferredStyle: .alert)
                let yes = UIAlertAction(title: "업데이트", style: .default, handler: updateAppHandler)
                let no = UIAlertAction(title: "다음에", style: .default, handler: cancelHandler)
                alertController.addAction(no)
                alertController.addAction(yes)
                present(alertController, animated: true, completion: nil)
//            }
        }  else {
            self.returnToView()
        }
    }
    
    func updateAppHandler(alert: UIAlertAction){
        let myAppId = "895405200" // 앱 아이디
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(myAppId)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func cancelHandler(alert: UIAlertAction){
        self.returnToView()
    }
    
    static func getDeviceIdentifier() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return identifier
    }
    
    static func iPhoneModel() -> String {

        let identifier = self.getDeviceIdentifier()

            switch identifier {
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":
                return "iPhone 4"
            case "iPhone4,1":
                return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":
                return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":
                return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":
                return "iPhone 5s"
            case "iPhone7,2":
                return "iPhone 6"
            case "iPhone7,1":
                return "iPhone 6 Plus"
            case "iPhone8,1":
                return "iPhone 6s"
            case "iPhone8,2":
                return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":
                return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":
                return "iPhone 7 Plus"
            case "iPhone8,4":
                return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":
                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":
                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":
                return "iPhone X"
            case "iPhone11,2":
                return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":
                return "iPhone XS Max"
            case "iPhone11,8":
                return "iPhone XR"
            case "iPhone12,1":
                return "iPhone 11"
            case "iPhone12,3":
                return "iPhone 11 Pro"
            case "iPhone12,5":
                return "iPhone 11 Pro Max"
            default:
                return identifier
            }
        }
}
