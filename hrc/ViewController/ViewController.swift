//
//  ViewController.swift
//  hrc
//
//  Created by Eun Bi Lim on 2019/12/27.
//  Copyright © 2019 Eun Bi Lim. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    @IBOutlet var webView: WKWebView!
    var popupWebView: WKWebView?
    var newWebView: WKWebView?
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
//    var mBaseUrl = "https://cxw.hrcglobal.com"
    var mBaseUrl = "https://cxw.hrcglobal.com/Home/JoinInfo"
//    let mBaseUrl = "http://192.168.0.127:49797/main"
//    let mBaseUrl = "http://jajuwa.epopcon.com:19080/public/apk/test_ota/index.html"
    var mUserId = UserDefaults.standard.string(forKey: "userId")
    var mUserPw = UserDefaults.standard.string(forKey: "userPw")
    var mUserPId = UserDefaults.standard.string(forKey: "userPid")
    var mCookie = UserDefaults.standard.string(forKey: "cookie")
    
    let imagePickerController = UIImagePickerController()
    
    let userData: UserData = UserData.shared
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let statusBar =  UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = UIColor.white
        
        setupWebView()

        let url = URL(string: mBaseUrl)
        var request = URLRequest(url: url!)
        if mCookie != "" && mCookie != nil {
            request.addValue(mCookie! , forHTTPHeaderField: "Cookie")
            UserDefaults.standard.set("", forKey: "cookie")
            UserDefaults.standard.synchronize()
        } else {
            WKWebView.clean()
        }
        webView.load(request)
    }
    
    func setupWebView() {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        var version: String {
            let dictionary = Bundle.main.infoDictionary
            let version = dictionary!["CFBundleShortVersionString"]
            return version as! String
        }
        
        let script = "javascript:function getAppVersion() { var appVersion = '\(version)'; return appVersion;};";
        let userScript = WKUserScript(source: script, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        let contentController = WKUserContentController()
        contentController.addUserScript(userScript)
        let config = WKWebViewConfiguration()
        
        contentController.add(self, name: "setUserInfo")
        contentController.add(self, name: "logout")
        contentController.add(self, name: "changePassword")
        config.userContentController = contentController
        
        config.preferences = preferences

        webView = WKWebView(frame: view.bounds, configuration: config)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = true

        view.addSubview(webView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let intro = segue.destination as? IntroViewController {
            intro.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //alert 처리
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in completionHandler() }))
        self.present(alertController, animated: true, completion: nil) }

    //confirm 처리
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in completionHandler(true) }))
        alertController.addAction(UIAlertAction(title: "취소", style: .default, handler: { (action) in completionHandler(false) }))

        self.present(alertController, animated: true, completion: nil)
    }

    //confirm 처리2
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in textField.text = defaultText }
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: {
                (action) in if let text = alertController.textFields?.first?.text { completionHandler(text)

            } else { completionHandler(defaultText) } }))
        alertController.addAction(UIAlertAction(title: "취소", style: .default, handler: {
            (action) in completionHandler(nil) }))
        self.present(alertController, animated: true, completion: nil)
    }

    // 새창으로 연결
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction  .targetFrame == nil {
            webView.load(navigationAction.request)
        }
         
        if navigationAction.request.description.starts(with: "http://m.map.daum.net/actions") {
            let url = URL(string: navigationAction.request.description)!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
            return nil
            
        } else {
            let frame = UIScreen.main.bounds
            newWebView = WKWebView(frame: frame, configuration: configuration)
            newWebView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            newWebView!.navigationDelegate = self
            newWebView!.uiDelegate = self

            view.addSubview(newWebView!)
            
            return newWebView!
        }
    }
    
    @available(iOS 8.0, *)
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.frame = CGRect(x: view.frame.midX-50, y: view.frame.midY-50, width: 100, height: 100)
        activityIndicator.color = UIColor.red
        activityIndicator.hidesWhenStopped = true
//        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    
    @available(iOS 8.0, *)
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
//        self.activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url, url.scheme != "http" && url.scheme != "https" {
            UIApplication.shared.openURL(url)
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    ///새창 닫기
    func webViewDidClose(webView: WKWebView) {
        newWebView?.removeFromSuperview()
        newWebView = nil
    }

//  중복적으로 리로드 방지
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) { webView.reload() }
    
}

extension ViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "setUserInfo" {
            let dict = message.body as! [String:AnyObject]
            let id = dict["id"] as! String
            let pw = dict["pw"] as! String
            let isAutoLogin = dict["isAutoLogin"]
            if isAutoLogin != nil {
                if  isAutoLogin as! Bool {
                    userData.setUserId(userId: id)
                    userData.setUserPw(userPw: pw)
            
                    let message = "id : " + id + ", pw : " + pw
                    showToast(message: message)
                }
            }
            
        } else if message.name == "logout" {
            userData.clearUserData()
            WKWebView.clean()
        } else if message.name == "changePassword" {
            let newPw = message.body as! String
            userData.setUserPw(userPw: newPw)
        }
    }
}

extension UIViewController {
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-100, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension WKWebView {
    class func clean() {
        guard #available(iOS 9.0, *) else {return}

        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)

        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                #if DEBUG
                    print("WKWebsiteDataStore record deleted:", record)
                #endif
            }
        }
    }
}

