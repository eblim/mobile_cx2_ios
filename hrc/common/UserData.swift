//
//  UserData.swift
//  hrc
//
//  Created by Eun Bi Lim on 2020/01/28.
//  Copyright © 2020 Eun Bi Lim. All rights reserved.
//

import Foundation

class UserData {
    public static let shared = UserData()

    private init() {
    }
    
    func clearUserData() {
        setUserId(userId: "")
        setUserPw(userPw: "")
        setAutoLogin(autoLogin: false)
        setUserPid(userPid: "")
        setUserName(userName: "")
        setUserEmail(userEmail: "")
        UserDefaults.standard.synchronize()
    }
    
    func saveUserData(userId: String, userPid: String, userName: String, userEmail: String) {
        setUserId(userId: userId)
        setUserPid(userPid: userPid)
        setUserName(userName: userName)
        setUserEmail(userEmail: userEmail)
        UserDefaults.standard.synchronize()
    }
    
    func getUserId() -> String {
        if UserDefaults.standard.string(forKey: "userId") == nil {
            if UserDefaults.standard.string(forKey: "id") != nil && UserDefaults.standard.string(forKey: "id") != "" {
                UserDefaults.standard.set(UserDefaults.standard.string(forKey: "id"), forKey: "userId")
                return UserDefaults.standard.string(forKey: "userId")!
            } else {
                if UserDefaults.standard.string(forKey: "id2") != nil && UserDefaults.standard.string(forKey: "id2") != "" {
                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "id2"), forKey: "userId")
                    return UserDefaults.standard.string(forKey: "userId")!
                }
                return ""
            }
        }
        
        return UserDefaults.standard.string(forKey: "userId")!
    }
    
    func setUserId(userId: String) {
        UserDefaults.standard.set(userId, forKey: "userId")
        UserDefaults.standard.synchronize()
    }
    
    func getUserPw() -> String {
        if UserDefaults.standard.string(forKey: "userPw") == nil{
            if UserDefaults.standard.string(forKey: "pw") != nil && UserDefaults.standard.string(forKey: "pw") != "" {
                UserDefaults.standard.set(UserDefaults.standard.string(forKey: "pw"), forKey: "userPw")
                return UserDefaults.standard.string(forKey: "userPw")!
            } else {
                if UserDefaults.standard.string(forKey: "pw2") != nil && UserDefaults.standard.string(forKey: "pw2") != "" {
                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "pw2"), forKey: "userPw")
                    return UserDefaults.standard.string(forKey: "userPw")!
                }
            }
            return ""
        }
        return UserDefaults.standard.string(forKey: "userPw")!
    }
    
    func setUserPw(userPw: String) {
        UserDefaults.standard.set(userPw, forKey: "userPw")
        UserDefaults.standard.synchronize()
    }
    
    func getPushId() -> String {
        if UserDefaults.standard.string(forKey: "pushId") == nil {
            return ""
        }
        return UserDefaults.standard.string(forKey: "pushId")!
    }
    
    func setPushId(pushId: String) {
        UserDefaults.standard.set(pushId, forKey: "pushId")
    }
    
    func getAutoLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: "autoLogin")
    }
    
    func setAutoLogin(autoLogin: Bool) {
        UserDefaults.standard.set(autoLogin, forKey: "autoLogin")
    }
    
    func getUserPid() -> String {
        if UserDefaults.standard.string(forKey: "userPid") == nil {
            if UserDefaults.standard.string(forKey: "Userid") != nil && UserDefaults.standard.string(forKey: "Userid") != "" {
                UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Userid"), forKey: "userPid")
                return UserDefaults.standard.string(forKey: "userPid")!
            }
               return ""
        }
        return UserDefaults.standard.string(forKey: "userPid")!
    }
    
    func setUserPid(userPid: String) {
        UserDefaults.standard.set(userPid, forKey: "userPid")
    }
    
    func getUserName() -> String {
        return UserDefaults.standard.string(forKey: "userName")!;
    }
    
    func setUserName(userName: String) {
        UserDefaults.standard.set(userName, forKey: "userName")
    }
    
    func getUserEmail() -> String {
        return UserDefaults.standard.string(forKey: "userEmail")!
    }
    
    func setUserEmail(userEmail: String) {
        UserDefaults.standard.set(userEmail, forKey: "userEmail")
    }
}
